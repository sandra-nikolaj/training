package com.niko.training.lottery.controllers;

import com.niko.training.lottery.model.CombinationProfitResult;
import com.niko.training.lottery.model.LotteryResult;
import com.niko.training.lottery.service.DataService;
import com.niko.training.lottery.service.DownloadService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.OperationNotSupportedException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Api(value = "/results", description = "Group of endpoints displaying, counting, or simulating outcomes from a list of results of a lottery")
@RestController
@RequestMapping("/results")
@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DataController extends BaseController {

    @Value("${lottery.file.url}")
    String fileURL;

    @Value("${lottery.file.path}")
    String filePath;

    final DownloadService downloadService;
    final DataService dataService;

    public DataController(DownloadService downloadService, DataService dataService) {
        this.downloadService = downloadService;
        this.dataService = dataService;
    }

    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }

    @ApiOperation(value = "Update locally stored results", notes = "Download all the results from a remote location and save locally for easy access", response = String.class)
    @GetMapping("/update")
    public String updateResults() throws MalformedURLException {
        log.info("Downloading new data from server...");
        downloadService.downloadRemoteFile(new URL(fileURL), filePath);

        return "OK";
    }

    @ApiOperation(value = "Show only N last results", response = List.class)
    @GetMapping("/show/last/{n}")
    public List<LotteryResult> getLast(@ApiParam(value = "Number of results to show") @PathVariable int n) {
        log.info("Retrieving {} last results...", n);
        return dataService.getLast(n);
    }

    @ApiOperation(value = "Count how much a combination could have won already", notes = "Show how many times the given combination has brought any winnings (by amount) and what is the overall profit", response = CombinationProfitResult.class)
    @GetMapping("/count/profit/{a},{b},{c},{d},{e},{f}")
    public CombinationProfitResult countProfitForGivenCombination(
            @ApiParam(value = "1st distinct number in the betting combination") @PathVariable int a,
            @ApiParam(value = "2nd distinct number in the betting combination") @PathVariable int b,
            @ApiParam(value = "3rd distinct number in the betting combination") @PathVariable int c,
            @ApiParam(value = "4th distinct number in the betting combination") @PathVariable int d,
            @ApiParam(value = "5th distinct number in the betting combination") @PathVariable int e,
            @ApiParam(value = "6th distinct number in the betting combination") @PathVariable int f) throws OperationNotSupportedException {
        log.info("Counting the rate of return for the following combination: [{} {} {} {} {} {}]...", a, b, c, d, e, f);
        List<Integer> combination = Arrays.asList(a, b, c, d, e, f);

        if (combination.stream().distinct().collect(Collectors.toList()).size() != 6) {
            log.error("Combination contains duplicates. Aborting...");
            throw new OperationNotSupportedException("Remove duplicate values from the combination, please.");
        }

        Collections.sort(combination);

        if (combination.get(0) < 1 || combination.get(5) > 49) {
            log.error("Combination contains values out of supported range. Aborting...");
            throw new OperationNotSupportedException("Please provide values between 1 and 49 included.");
        }

        return dataService.findCombinationProfit(Arrays.asList(a, b, c, d, e, f));
    }

    @ApiOperation(value = "Find random numbers' frequency in given periods of time", notes = "Show how many times a number from a random combination was drawn grouped by predefined period", response = Map.class)
    @GetMapping("/histogram")
    public Map<Integer, Map<DataService.Range, Double>> countCombinationNumbersFrequency() {
        log.info("Randomly choosing 6 numbers between 1 - 49 and counting their frequency...");
        return dataService.findFrequencyForNumbersInRandomCombination();
    }
}
