package com.niko.training.lottery.converters;

import com.niko.training.lottery.model.LotteryResult;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class LotteryResultConverter implements Converter<String, LotteryResult> {

    public static final String dateTimeFormat = "dd.MM.yyyy";

    @Override
    public LotteryResult convert(String s) {
        String[] elements = s.split(" ");

        return new LotteryResult(
                Long.valueOf(elements[0].replaceAll("\\.", "")),
                LocalDate.parse(elements[1], DateTimeFormatter.ofPattern(dateTimeFormat)),
                Stream.of(elements[2].split(",")).map(Integer::valueOf).collect(Collectors.toList()));
    }
}
