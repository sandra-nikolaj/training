package com.niko.training.lottery.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Map;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
public class CombinationProfitResult {

    Map<Integer, Integer> winnersMap;
    Integer totalWinnings;

}
