package com.niko.training.lottery.service;

import com.niko.training.lottery.converters.LotteryResultConverter;
import com.niko.training.lottery.model.CombinationProfitResult;
import com.niko.training.lottery.model.LotteryResult;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DataService {

    public enum Range {
        ALL, YEAR, N
    }

    @Value("${lottery.file.path}")
    String filePath;

    @Value("${lottery.ticket.price}")
    Integer ticketPrice;

    final Map<Integer, Integer> resultPrizeMap = new HashMap<>();

    @Value("${lottery.frequency.yearCount}")
    Integer rangeYearCount;

    @Value("${lottery.frequency.n}")
    Integer rangeN;

    LotteryResultConverter lotteryResultConverter;

    public DataService(LotteryResultConverter lotteryResultConverter) {
        resultPrizeMap.put(3, 24);
        resultPrizeMap.put(4, 170);
        resultPrizeMap.put(5, 5000);
        resultPrizeMap.put(6, 6000000);

        this.lotteryResultConverter = lotteryResultConverter;
    }

    void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    void setTicketPrice(Integer ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    void setRangeYearCount(Integer rangeYearCount) {
        this.rangeYearCount = rangeYearCount;
    }

    void setRangeN(Integer rangeN) {
        this.rangeN = rangeN;
    }

    private List<String> readFile(String source) {
        List<String> fileLineList = Collections.emptyList();

        try {
            fileLineList = Files.readAllLines(Paths.get(source));
        } catch (IOException e) {
            log.error("An error has occurred during file read. Check the below stack for more information:");
            e.printStackTrace();
        }

        return fileLineList;
    }

    public List<LotteryResult> getAll() {
        log.info("Trying to read data from the file...");
        List<String> resultList = readFile(filePath);

        log.info("Converting string data to objects...");
        return resultList.stream().map(lotteryResultConverter::convert).collect(Collectors.toList());
    }

    public List<LotteryResult> getLast(int n) {
        List<LotteryResult> lotteryResultList = getAll();

        if (n > lotteryResultList.size()) {
            log.info("Given number exceeds the number of results. Showing all...");
            return lotteryResultList;
        }

        log.info("Trimming the list to <{}> last results...", n);
        return lotteryResultList.stream().skip(lotteryResultList.size() - n).collect(Collectors.toList());
    }

    private Map<Integer, Integer> findCountOfWinners(List<LotteryResult> lotteryResultList, List<Integer> combination) {
        log.info("Building a map with <countOfGoodHitsInACombination> as a key and <listOfCombinationsWith<key>GoodHits> as value...");
        Map<Integer, List<LotteryResult>> winningNumbersCombinationsMap = lotteryResultList.stream()
                .collect(Collectors.groupingBy(e ->
                        e.getResults().stream().filter(combination::contains).collect(Collectors.toList()).size()
                ));

        log.info("Removing keys that do not earn money. Converting the map to keep the size instead of the full list (count of good combinations)...");
        return winningNumbersCombinationsMap.entrySet().stream().filter(e -> e.getKey() > 2)
                .collect(Collectors.toMap(Map.Entry::getKey, f -> f.getValue().size()));
    }

    private Integer countWinningsForCombination(Map<Integer, Integer> winningNumbersCountMap) {
        log.info("Calculating total winnings. For every paying result multiply the count of combinations by prize and add to the accumulator...");
        return winningNumbersCountMap.keySet().stream()
                .reduce(0, (f, g) -> f + (resultPrizeMap.get(g) * winningNumbersCountMap.get(g)));
    }

    public CombinationProfitResult findCombinationProfit(List<Integer> combination) {
        List<LotteryResult> lotteryResultList = getAll();
        Map<Integer, Integer> winningNumbersCountMap = findCountOfWinners(lotteryResultList, combination);

        Integer totalSpendings = ticketPrice * lotteryResultList.size();
        Integer totalWinnings = countWinningsForCombination(winningNumbersCountMap);

        return new CombinationProfitResult(winningNumbersCountMap, totalWinnings - totalSpendings);
    }

    public List<Integer> generateRandomCombination() {
        log.info("Generating a 6 numbers long combination of random Integers between 1 - 49 without duplicates...");
        return new Random().ints(1, 49).distinct().limit(6).sorted().boxed().collect(Collectors.toList());
    }

    private static Collector<LotteryResult, Map<Range, List<Integer>>, Map<Range, List<Integer>>> rangeToNumberResultsMapCollector(LocalDate aYearAgo, Long idAbove) {
        return Collector.of(
                HashMap::new,
                (map, value) -> {
                    map.computeIfAbsent(Range.ALL, k -> new ArrayList<>());
                    map.computeIfAbsent(Range.YEAR, k -> new ArrayList<>());
                    map.computeIfAbsent(Range.N, k -> new ArrayList<>());

                    map.get(Range.ALL).addAll(value.getResults());

                    if (value.getLocalDate().compareTo(aYearAgo) > 0) {
                        map.get(Range.YEAR).addAll(value.getResults());
                    }

                    if (value.getId() > idAbove) {
                        map.get(Range.N).addAll(value.getResults());
                    }
                },
                (r1, r2) -> {
                    throw new UnsupportedOperationException("Parallel processing not supported");
                }
        );
    }

    public Map<Integer, Map<Range, Double>> findFrequencyForNumbersInRandomCombination() {
        List<Integer> randomCombination = generateRandomCombination();
        List<LotteryResult> lotteryResultList = getAll();

        // List<Integer> allDrawnNumbersList = lotteryResultList.stream().map(LotteryResult::getResults).flatMap(Collection::stream).collect(Collectors.toList());
        // Map<Integer, Long> combinationNumberToOccurrencesMap = allDrawnNumbersList.stream().filter(randomCombination::contains).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        // Map<Integer, Double> numberToPercentageMap = combinationNumberToOccurrencesMap.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, f -> Math.round(f.getValue() * 100.00 / allDrawnNumbersList.size() * 100) / 100.00));

        LocalDate aYearAgo = LocalDate.now().minusYears(rangeYearCount);

        log.info("Building a map of lists of drawn numbers within a specified (predefined) period of time...");
        Map<Range, List<Integer>> rangeToResultNumbersMap = lotteryResultList.stream()
                .collect(rangeToNumberResultsMapCollector(aYearAgo, lotteryResultList
                        .get(lotteryResultList.size() - 1 - rangeN).getId()));

        log.info("Replacing the list of drawn numbers with a percentage of hits of the given number in that list...");
        log.info("Putting the <range> to <frequency> map under corresponding number...");
        Map<Integer, Map<Range, Double>> collect = randomCombination.stream().collect(Collectors
                .toMap(k -> k, e -> rangeToResultNumbersMap.entrySet().stream().collect(Collectors
                        .toMap(Map.Entry::getKey, f -> Math
                                .round(Collections.frequency(f.getValue(), e) * 100.00 / f.getValue()
                                        .size() * 100) / 100.00))));

        return collect;
    }

}
