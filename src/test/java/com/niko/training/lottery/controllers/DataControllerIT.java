package com.niko.training.lottery.controllers;

import com.niko.training.lottery.Application;
import io.restassured.RestAssured;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class DataControllerIT {

    @Before
    public void setUp() {
        RestAssured.baseURI = "http://localhost/results";
        // RestAssured.registerParser("text/plain", Parser.JSON);
    }

    @Test
    public void shouldDownloadTheFileAndShowOK() {
        RestAssured.when().get("/update").then().statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void shouldReturnXElements() {
        RestAssured.when().get("/show/last/5").then().statusCode(HttpStatus.SC_OK).body("$", Matchers.hasSize(5));
    }

    @Test
    public void shouldReturnProfitForCombination() {
        RestAssured.when().get("/count/profit/8,12,31,39,43,45").then().statusCode(HttpStatus.SC_OK)
                .body("winnersMap.3", Matchers.equalTo(93))
                .body("winnersMap.4", Matchers.equalTo(5))
                .body("winnersMap.6", Matchers.equalTo(1))
                .body("totalWinnings", Matchers.equalTo(5984476));
    }

    @Test
    public void shouldReturnProfitForCombination_TrowhErrorWhenDuplicates() {
        RestAssured.when().get("/count/profit/8,8,31,39,43,45").then().statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }

    @Test
    public void shouldReturnProfitForCombination_ThrowErrorWhenOutOfRange() {
        RestAssured.when().get("/count/profit/8,12,31,39,43,51").then().statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }

    @Test
    public void shouldReturnFrequencyOfANumberInPredefinedRangesOfTime() {
        RestAssured.when().get("/histogram").then().statusCode(HttpStatus.SC_OK)
                .body("$", Matchers.isA(HashMap.class));
    }
}
