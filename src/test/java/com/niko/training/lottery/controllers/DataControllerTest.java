package com.niko.training.lottery.controllers;

import com.niko.training.lottery.model.CombinationProfitResult;
import com.niko.training.lottery.model.LotteryResult;
import com.niko.training.lottery.service.DataService;
import com.niko.training.lottery.service.DownloadService;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.naming.OperationNotSupportedException;
import java.net.MalformedURLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class DataControllerTest {

    @InjectMocks
    DataController dataController;

    @Mock
    DownloadService downloadService;

    @Mock
    DataService dataService;

    List<LotteryResult> lotteryResultList;

    @Before
    public void setUp() {
        lotteryResultList = new ArrayList<>();
        lotteryResultList.add(new LotteryResult(1L, LocalDate.of(1990, 1, 25), Arrays.asList(1, 2, 3, 4, 5, 6)));
        lotteryResultList.add(new LotteryResult(2L, LocalDate.of(1995, 4, 15), Arrays.asList(2, 4, 6, 7, 10, 12)));
        lotteryResultList.add(new LotteryResult(3L, LocalDate.of(2000, 8, 11), Arrays.asList(3, 6, 9, 12, 15, 18)));
    }

    @Test
    public void updateResults() throws MalformedURLException {
        dataController.setFileURL("http://www.mbnet.com.pl/dl.txt");

        String result = dataController.updateResults();

        Assertions.assertThat(result).isNotNull().isNotEmpty().isEqualTo("OK");
    }

    @Test
    public void updateResults_MissingURL() {
        Assertions.assertThatThrownBy(() -> dataController.updateResults())
                .isInstanceOf(MalformedURLException.class);
    }

    @Test
    public void getLast() {
        Mockito.when(dataService.getLast(Mockito.eq(2))).thenReturn(lotteryResultList.subList(1, 3));

        List<LotteryResult> result = dataController.getLast(2);

        Assertions.assertThat(result).isNotNull().isNotEmpty().hasSize(2);
        Assertions.assertThat(result).extracting("id").contains(2L, 3L);
    }

    @Test
    public void countProfitForGivenCombination() throws OperationNotSupportedException {
        Map<Integer, Integer> hitCountToWinningCombinationsCountMap = new HashMap<>();
        hitCountToWinningCombinationsCountMap.put(6, 1);
        hitCountToWinningCombinationsCountMap.put(3, 1);

        CombinationProfitResult combinationProfitResult = new CombinationProfitResult(hitCountToWinningCombinationsCountMap, 6000024);

        Mockito.when(dataService.findCombinationProfit(Arrays.asList(1, 2, 3, 4, 5, 6))).thenReturn(combinationProfitResult);

        CombinationProfitResult result = dataController.countProfitForGivenCombination(1, 2, 3, 4, 5, 6);

        Assertions.assertThat(result).isNotNull();
        Assertions.assertThat(result.getWinnersMap()).isEqualTo(hitCountToWinningCombinationsCountMap);
        Assertions.assertThat(result.getTotalWinnings()).isEqualTo(6000024);
    }

    @Test
    public void countProfitForGivenCombination_DuplicateValues() {
        Assertions.assertThatThrownBy(() -> dataController.countProfitForGivenCombination(1, 2, 1, 4, 5, 6))
                .isInstanceOf(OperationNotSupportedException.class)
                .hasMessage("Remove duplicate values from the combination, please.");
    }

    @Test
    public void countProfitForGivenCombination_WrongValues() {
        Assertions.assertThatThrownBy(() -> dataController.countProfitForGivenCombination(-1, 2, 3, 4, 5, 6))
                .isInstanceOf(OperationNotSupportedException.class)
                .hasMessage("Please provide values between 1 and 49 included.");
    }

    @Test
    public void countCombinationNumbersFrequency() {
        Map<Integer, Map<DataService.Range, Double>> expectedMap = new HashMap<>();
        Map<DataService.Range, Double> expectedInMap = new HashMap<>();

        expectedInMap.put(DataService.Range.N, 20.0);
        expectedMap.put(21, expectedInMap);

        Mockito.when(dataController.countCombinationNumbersFrequency()).thenReturn(expectedMap);

        Map<Integer, Map<DataService.Range, Double>> result = dataController.countCombinationNumbersFrequency();

        Assertions.assertThat(result).isNotNull().isNotEmpty().hasSize(1);
        Assertions.assertThat(result.get(21)).isNotNull().isNotEmpty().hasSize(1);
        Assertions.assertThat(result.get(21).get(DataService.Range.N)).isEqualTo(20);
    }
}