package com.niko.training.lottery.service;

import com.niko.training.lottery.converters.LotteryResultConverter;
import com.niko.training.lottery.model.CombinationProfitResult;
import com.niko.training.lottery.model.LotteryResult;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@FieldDefaults(level = AccessLevel.PRIVATE)
@RunWith(MockitoJUnitRunner.class)
public class DataServiceTest {

    @Spy
    @InjectMocks
    DataService dataService;

    @Mock
    LotteryResultConverter lotteryResultConverter;

    List<LotteryResult> lotteryResultList;

    @Before
    public void setUp() {
        dataService.setFilePath("downloads/tests/dl.txt");
        dataService.setTicketPrice(3);
        dataService.setRangeYearCount(1);
        dataService.setRangeN(3);

        lotteryResultList = new ArrayList<>();
        lotteryResultList.add(new LotteryResult(1L, LocalDate.of(1990, 1, 25), Arrays.asList(1, 2, 3, 4, 5, 6)));
        lotteryResultList.add(new LotteryResult(2L, LocalDate.of(1995, 4, 15), Arrays.asList(2, 4, 6, 7, 10, 12)));
        lotteryResultList.add(new LotteryResult(3L, LocalDate.of(2000, 8, 11), Arrays.asList(3, 6, 9, 12, 15, 18)));
        lotteryResultList.add(new LotteryResult(4L, LocalDate.of(2005, 3, 31), Arrays.asList(4, 8, 14, 18, 22, 26)));
        lotteryResultList.add(new LotteryResult(5L, LocalDate.of(2010, 4, 12), Arrays.asList(5, 10, 15, 20, 25, 30)));
        lotteryResultList.add(new LotteryResult(6L, LocalDate.of(2010, 10, 5), Arrays.asList(6, 12, 18, 24, 30, 36)));
        lotteryResultList.add(new LotteryResult(7L, LocalDate.of(2018, 4, 27), Arrays.asList(7, 10, 13, 16, 19, 22)));
        lotteryResultList.add(new LotteryResult(8L, LocalDate.of(2018, 6, 30), Arrays.asList(8, 12, 16, 20, 24, 28)));
        lotteryResultList.add(new LotteryResult(9L, LocalDate.of(2018, 9, 17), Arrays.asList(9, 10, 11, 12, 13, 14)));
        lotteryResultList.add(new LotteryResult(10L, LocalDate.of(2018, 10, 2), Arrays.asList(10, 18, 27, 34, 41, 49)));

        Mockito.when(lotteryResultConverter.convert("1. 25.01.1990 1,2,3,4,5,6")).thenReturn(lotteryResultList.get(0));
        Mockito.when(lotteryResultConverter.convert("2. 15.04.1995 2,4,6,7,10,12")).thenReturn(lotteryResultList.get(1));
        Mockito.when(lotteryResultConverter.convert("3. 11.08.2000 3,6,9,12,15,18")).thenReturn(lotteryResultList.get(2));
        Mockito.when(lotteryResultConverter.convert("4. 31.03.2005 4,8,14,18,22,26")).thenReturn(lotteryResultList.get(3));
        Mockito.when(lotteryResultConverter.convert("5. 12.04.2010 5,10,15,20,25,30")).thenReturn(lotteryResultList.get(4));
        Mockito.when(lotteryResultConverter.convert("6. 05.10.2010 6,12,18,24,30,36")).thenReturn(lotteryResultList.get(5));
        Mockito.when(lotteryResultConverter.convert("7. 27.04.2018 7,10,13,16,19,22")).thenReturn(lotteryResultList.get(6));
        Mockito.when(lotteryResultConverter.convert("8. 30.06.2018 8,12,16,20,24,28")).thenReturn(lotteryResultList.get(7));
        Mockito.when(lotteryResultConverter.convert("9. 17.09.2018 9,10,11,12,13,14")).thenReturn(lotteryResultList.get(8));
        Mockito.when(lotteryResultConverter.convert("10. 02.10.2018 10,18,27,34,41,49")).thenReturn(lotteryResultList.get(9));
    }

    @Test
    public void getAll() {
        List<LotteryResult> result = dataService.getAll();

        Assertions.assertThat(result).isNotNull().isNotEmpty().hasSize(10);
        Assertions.assertThat(result).extracting("id").contains(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L);
        Assertions.assertThat(result).isEqualTo(lotteryResultList);

        Mockito.verify(lotteryResultConverter, Mockito.times(10)).convert(Mockito.any());
    }

    @Test
    public void getAll_MissingFile() {
        dataService.setFilePath("downloads/tests/dl2.txt");

        List<LotteryResult> result = dataService.getAll();

        Assertions.assertThat(result).isNotNull().isEmpty();

        Mockito.verify(lotteryResultConverter, Mockito.times(0)).convert(Mockito.any());
    }

    @Test
    public void getLast() {
        List<LotteryResult> nLastLotteryResultsList = lotteryResultList.subList(7, 10);

        List<LotteryResult> result = dataService.getLast(3);

        Assertions.assertThat(result).isNotNull().isNotEmpty().hasSize(3);
        Assertions.assertThat(result).extracting("id").contains(8L, 9L, 10L).doesNotContain(1L, 2L, 3L, 4L, 5L, 6L, 7L);
        Assertions.assertThat(result).isEqualTo(nLastLotteryResultsList);

        Mockito.verify(lotteryResultConverter, Mockito.times(10)).convert(Mockito.any());
    }

    @Test
    public void getLast_NotEnoughResults() {
        List<LotteryResult> result = dataService.getLast(11);

        Assertions.assertThat(result).isNotNull().isNotEmpty().hasSize(10);
        Assertions.assertThat(result).extracting("id").contains(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L);
        Assertions.assertThat(result).isEqualTo(lotteryResultList);

        Mockito.verify(lotteryResultConverter, Mockito.times(10)).convert(Mockito.any());
    }

    @Test
    public void findCombinationProfit() {
        // 1, 2, 3, 4, 5, 6
        // hits - combinations - count
        // 1 - 4,8,14,18,22,26 | 5,10,15,20,25,30 | 6,12,18,24,30,36 - 3
        // 2 - 3,6,9,12,15,18 - 1
        // 3 - 2,4,6,7,10,12 - 1
        // 4 - X - 0
        // 5 - X - 0
        // 6 - 1,2,3,4,5,6 - 1
        //
        // 24 + 6000000

        Map<Integer, Integer> hitCountToWinningCombinationsCountMap = new HashMap<>();
        hitCountToWinningCombinationsCountMap.put(6, 1);
        hitCountToWinningCombinationsCountMap.put(3, 1);

        CombinationProfitResult result = dataService.findCombinationProfit(Arrays.asList(1, 2, 3, 4, 5, 6));

        Assertions.assertThat(result).isNotNull();
        Assertions.assertThat(result.getWinnersMap()).isNotNull().isNotEmpty().isEqualTo(hitCountToWinningCombinationsCountMap);
        Assertions.assertThat(result.getTotalWinnings()).isNotNull().isEqualTo((6000000 + 24) - (10 * 3));

        Mockito.verify(lotteryResultConverter, Mockito.times(10)).convert(Mockito.any());
    }

    @Test
    public void findCombinationProfit_NoHits() {
        CombinationProfitResult result = dataService.findCombinationProfit(Arrays.asList(44, 45, 46, 47, 48, 49));

        Assertions.assertThat(result).isNotNull();
        Assertions.assertThat(result.getWinnersMap()).isNullOrEmpty();
        Assertions.assertThat(result.getTotalWinnings()).isNegative().isEqualTo(10 * 3 * -1);

        Mockito.verify(lotteryResultConverter, Mockito.times(10)).convert(Mockito.any());
    }

    @Test
    public void findFrequencyForNumbersInRandomCombination() {
        // 3, 7, 11, 15, 23, 42
        // 3  - 2 | 0
        // 7  - 2 | 1 | 0
        // 11 - 1 | 1 | 1
        // 15 - 2 | 0
        // 23 - 0
        // 42 - 0
        //
        // 60 | 24 | 18

        List<Integer> combination = Arrays.asList(3, 7, 11, 15, 23, 42);
        Map<Integer, Map<DataService.Range, Double>> numberToFrequencyMapMap = new HashMap<>();

        combination.forEach(e -> {
            Map<DataService.Range, Double> rangeToFrequencyMap = new HashMap<>();

            switch (e) {
                case 3:
                case 15:
                    rangeToFrequencyMap.put(DataService.Range.ALL, calcPercentage(2, 60));
                    rangeToFrequencyMap.put(DataService.Range.YEAR, calcPercentage(0, 24));
                    rangeToFrequencyMap.put(DataService.Range.N, calcPercentage(0, 18));
                    break;
                case 7:
                    rangeToFrequencyMap.put(DataService.Range.ALL, calcPercentage(2, 60));
                    rangeToFrequencyMap.put(DataService.Range.YEAR, calcPercentage(1, 24));
                    rangeToFrequencyMap.put(DataService.Range.N, calcPercentage(0, 18));
                    break;
                case 11:
                    rangeToFrequencyMap.put(DataService.Range.ALL, calcPercentage(1, 60));
                    rangeToFrequencyMap.put(DataService.Range.YEAR, calcPercentage(1, 24));
                    rangeToFrequencyMap.put(DataService.Range.N, calcPercentage(1, 18));
                    break;
                case 23:
                case 42:
                    rangeToFrequencyMap.put(DataService.Range.ALL, calcPercentage(0, 60));
                    rangeToFrequencyMap.put(DataService.Range.YEAR, calcPercentage(0, 24));
                    rangeToFrequencyMap.put(DataService.Range.N, calcPercentage(0, 18));
                    break;
            }

            numberToFrequencyMapMap.put(e, rangeToFrequencyMap);
        });

        Mockito.when(dataService.generateRandomCombination()).thenReturn(Arrays.asList(3, 7, 11, 15, 23, 42));

        Map<Integer, Map<DataService.Range, Double>> result = dataService.findFrequencyForNumbersInRandomCombination();

        System.out.println(result);

        Assertions.assertThat(result).isNotNull();
        Assertions.assertThat(result.keySet()).isNotEmpty().contains(3, 7, 11, 15, 23, 42);
        Assertions.assertThat(result.get(11)).isNotNull().isNotEmpty().isEqualTo(numberToFrequencyMapMap.get(11));

        Mockito.verify(lotteryResultConverter, Mockito.times(10)).convert(Mockito.any());
        Mockito.verify(dataService).generateRandomCombination();
    }

    private Double calcPercentage(int x, int all) {
        return Math.round((x * 100.00) / all * 100) / 100.00;
    }
}